import { EOL } from 'os'

/**
 * @type {_forkfeed.forkfeed}
 */
const forkFeed = (readable, stdin, inputs = [], log = null, eol = EOL) => {
  if (log) readable.on('data', d => log.write(d))

  let [a, ...rest] = inputs
  if (!a) return

  const handler = (d) => {
    const [regexp, answer] = a
    if (!regexp.test(d)) return

    const an = `${answer}${eol}`
    if (log) log.write(an)

    stdin.write(an)
    ;([a, ...rest] = rest)
    if (!a) readable.removeListener('data', handler)
  }
  readable.on('data', handler)
}

export default forkFeed

/**
 * @suppress {nonStandardJsDocs}
 * @typedef {import('../')} _forkfeed.forkfeed
 */