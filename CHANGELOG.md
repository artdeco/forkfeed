## 9 April 2020

### [1.2.0](https://gitlab.com/artdeco/forkfeed/compare/v1.1.1...v1.2.0)

- [package] Move to _GitLab_.
- [feature] Pass `os.EOL` and configure it via args.

## 29 April 2019

### [1.1.1](https://gitlab.com/artdeco/forkfeed/compare/v1.1.0...v1.1.1)

- [package] Move the repository to `contexttesting`; add `src` to the files list.

### [1.1.0](https://gitlab.com/artdeco/forkfeed/compare/v1.0.0...v1.1.0)

- [package] Publish the `module` field in _package.json_.
- [types] Correct types for the _GCC_.

## 7 October 2018

### 1.0.0

- Create `forkfeed` with [`mnp`][https://mnpjs.org]
- [repository]: `src`, `test`
