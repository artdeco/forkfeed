<div align="center">

# ForkFeed

[![npm version](https://badge.fury.io/js/forkfeed.svg)](https://www.npmjs.com/package/forkfeed)
<a href="https://gitlab.com/artdeco/forkfeed/-/commits/master">
  <img src="https://gitlab.com/artdeco/forkfeed/badges/master/pipeline.svg" alt="Pipeline Badge">
</a>
</div>

`forkfeed` is a function that passes answer values to a child process on data. For example, if a fork was spawned which requires user interaction, this module can be used to pass answers to it according to the seen data from `stdout`.

```sh
yarn add forkfeed
npm i forkfeed
```

## Table Of Contents

- [Table Of Contents](#table-of-contents)
- [API](#api)
- [`forkfeed(readable, stdin, inputs, log=, eol=): void`](#forkfeedreadable-streamreadablestdin-streamwritableinputs-arrayarrayregexpstringlog-streamwritableeol-string-void)
- [Copyright & License](#copyright--license)

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/0.svg?sanitize=true">
</a></div>

## API

The package is available by importing its default function:

```js
import forkFeed from 'forkfeed'
```

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/1.svg?sanitize=true">
</a></div>

## <code><ins>forkfeed</ins>(</code><sub><br/>&nbsp;&nbsp;`readable: !stream.Readable,`<br/>&nbsp;&nbsp;`stdin: !stream.Writable,`<br/>&nbsp;&nbsp;`inputs: !Array<!Array<(!RegExp|string)>>,`<br/>&nbsp;&nbsp;`log=: stream.Writable,`<br/>&nbsp;&nbsp;`eol=: string,`<br/></sub><code>): <i>void</i></code>
Write data to the `writable` when data from the `readable` matches the regexp.

 - <kbd><strong>readable*</strong></kbd> <em><code><a href="https://nodejs.org/api/stream.html#stream_class_stream_readable" title="A stream that emits data (an external source of data that pushes new chunks as they are ready)."><img src=".documentary/type-icons/node.png" alt="Node.JS Docs">!stream.Readable</a></code></em>: A readable stream to detect data on.
 - <kbd><strong>stdin*</strong></kbd> <em><code><a href="https://nodejs.org/api/stream.html#stream_class_stream_writable" title="A stream that can be written data to."><img src=".documentary/type-icons/node.png" alt="Node.JS Docs">!stream.Writable</a></code></em>: A writable stream to pass answers to.
 - <kbd><strong>inputs*</strong></kbd> <em><code>!Array&lt;!Array&lt;(!RegExp \| string)&gt;&gt;</code></em>: A serial collection of answers. Each answer will be ended with an EOL character (unless specified otherwise in the last argument). For example, pass `[[/question/, 'answer'], [/question2/, 'answer2]]`.
 - <kbd>log</kbd> <em><code><a href="https://nodejs.org/api/stream.html#stream_class_stream_writable" title="A stream that can be written data to."><img src=".documentary/type-icons/node.png" alt="Node.JS Docs">stream.Writable</a></code></em> (optional): A stream to which to write both data from readable, and the passed answer. Default `null`.
 - <kbd>eol</kbd> <em>`string`</em> (optional): The line feed to pass at the end of each answer. By default, _ForkFeed_ uses `os.EOL`, and when readline interface is used, there won't be `\r` in answers on Windows, but this can be useful for logging.

Sets up a listener on the _Readable_ stream and writes answers to the _Writable_ stream when data specified in `inputs` was detected. The logging stream will receive both data and answers.



Given a fork source code as

```js
const rl = require('readline')

const i = rl.createInterface({
  input: process.stdin,
  output: process.stderr,
})
i.question(
  'What was the football coach yelling at the vending machine?\n> ',
  () => {
    i.question('What do snowmen do in their spare time?\n> ', () => {
      process.exit(1)
    })
  })
```

The function can be used in the following manner:

```js
/* yarn example/ */
import forkFeed from 'forkfeed'
import { fork } from 'child_process'

(async () => {
  const cp = fork('example/fork', [], { stdio: 'pipe' })
  forkFeed(cp.stderr, cp.stdin, [
    [/coach/, 'Gimme my quarter back!!!'],
    [/snowmen/, 'Just chilling.'],
  ], process.stdout)
})()
```
```
What was the football coach yelling at the vending machine?
> Gimme my quarter back!!!
What do snowmen do in their spare time?
> Just chilling.
```

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/2.svg?sanitize=true">
</a></div>

## Copyright & License

GNU Affero General Public License v3.0

<table>
  <tr>
    <th>
      <a href="https://www.artd.eco">
        <img width="100" src="https://gitlab.com/uploads/-/system/group/avatar/7454762/artdeco.png"
          alt="Art Deco">
      </a>
    </th>
    <th>© <a href="https://www.artd.eco">Art Deco™</a> for <a href="https://www.contexttesting.com">ContextTesting</a> 2020</th>
    <th>
      <a href="https://www.contexttesting.com">
        <img src="https://avatars1.githubusercontent.com/u/44418436?s=100" width="100" alt="ContextTesting">
      </a>
    </th>
    <th><a href="LICENSE"><img src=".documentary/agpl-3.0.svg" alt="AGPL-3.0"></a></th>
  </tr>
</table>

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/-1.svg?sanitize=true">
</a></div>