import { resolve } from 'path'
import { debuglog } from 'util'
import { EOL } from 'os'

const LOG = debuglog('forkfeed')

const FIXTURE = resolve(__dirname, '../fixture')

/**
 * A testing context for the package.
 */
export default class Context {
  async _init() {
    LOG('init context')
  }
  /**
   * Example method.
   */
  example() {
    return 'OK'
  }
  /**
   * Path to the fixture file.
   */
  get FIXTURE() {
    return resolve(FIXTURE, 'test.txt')
  }
  get SNAPSHOT_DIR() {
    return resolve(__dirname, '../snapshot')
  }
  async _destroy() {
    LOG('destroy context')
  }
  get crlf() {
    return crlf
  }
}

export function crlf(strings, ...args) {
  const raw = strings.raw.map((r) => {
    return r.replace(/\r?\n/g, EOL)
  })
  const s = joinRaw(raw, ...args)
  return s
}

const joinRaw = (raw, ...args) => {
  const f = raw.reduce((acc, s, i) => {
    s = s.replace(/\\`/g, '`')
    acc.push(s)
    const a = args[i]
    if (a) acc.push(a)
    return acc
  }, []).join('')
  return f
}
