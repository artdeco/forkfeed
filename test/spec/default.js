import { equal } from 'assert'
import { Readable } from 'stream'
import Catchment from 'catchment'
import Context from '../context'
import forkFeed from '../../src'

/** @type {Object<string, (c: Context)>} */
const ts = {
  context: Context,
  async'can setup answers'({ crlf }) {
    const rs = new Readable({
      read() {
        this.push('data: ')
        this.push('world: ')
        this.push(null)
      },
    })
    const c = new Catchment()
    const l = new Catchment()
    forkFeed(rs, c, [
      [/data/, 'ok data'],
      [/world/, 'hello data'],
    ], l)
    rs.on('end', () => {
      c.end()
      l.end()
    })
    const cRes = await c.promise
    const lRes = await l.promise
    equal(cRes, crlf`ok data
hello data
`)
    equal(lRes, crlf`data: ok data
world: hello data
`)
  },
  async'can setup answers without log'({ crlf }) {
    const rs = new Readable({
      read() {
        this.push('data: ')
        this.push('world: ')
        this.push(null)
      },
    })
    const c = new Catchment()
    forkFeed(rs, c, [
      [/data/, 'ok data'],
      [/world/, 'hello data'],
    ])
    rs.on('end', () => {
      c.end()
    })
    const cRes = await c.promise
    equal(cRes, crlf`ok data
hello data
`)
  },
  async'works when answers are exhausted'({ crlf }) {
    const rs = new Readable({
      read() {
        this.push('data: ')
        this.push('thank you.')
        this.push(null)
      },
    })
    const c = new Catchment()
    const l = new Catchment()
    forkFeed(rs, c, [
      [/data/, 'ok data'],
    ], l)
    rs.on('end', () => {
      c.end()
      l.end()
    })
    const cRes = await c.promise
    const lRes = await l.promise
    equal(cRes, crlf`ok data
`)
    equal(lRes, crlf`data: ok data
thank you.`)
  },
}

export default ts