import { repository } from '../package.json'
import { format } from 'url'

const PipelineBadge = ({ version = 'master' }) => {
  const r = repository.replace('gitlab:', '')
  const badge = format({
    protocol: 'https',
    host: 'gitlab.com',
    pathname: `${r}/badges/${version}/pipeline.svg`
  })
  const commits = format({
    protocol: 'https',
    host: 'gitlab.com',
    pathname: `${r}/-/commits/${version}`
  })
  return (<a href={commits}><img src={badge} alt="Pipeline Badge"/></a>)
}

export default {
  'pipeline-badge': PipelineBadge
}