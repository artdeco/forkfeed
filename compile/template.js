const { $forkfeed } = require('./forkfeed')

/**
 * @methodType {_forkfeed.forkfeed}
 */
function forkfeed(readable, stdin, inputs, log, eol) {
  return $forkfeed(readable, stdin, inputs, log, eol)
}

module.exports = forkfeed

/* documentary types/index.xml namespace */
