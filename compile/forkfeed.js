#!/usr/bin/env node
'use strict';
const os = require('os');             const g=os.EOL;module.exports={$forkfeed:(d,h,k=[],b=null,l=g)=>{if(b)d.on("data",a=>b.write(a));let [c,...e]=k;if(c){var f=a=>{const [m,n]=c;m.test(a)&&(a=`${n}${l}`,b&&b.write(a),h.write(a),[c,...e]=e,c||d.removeListener("data",f))};d.on("data",f)}}};

//# sourceMappingURL=forkfeed.js.map