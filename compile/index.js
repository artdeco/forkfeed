const { $forkfeed } = require('./forkfeed')

/**
 * Write data to the `writable` when data from the `readable` matches the regexp.
 * @param {!stream.Readable} readable A readable stream to detect data on.
 * @param {!stream.Writable} stdin A writable stream to pass answers to.
 * @param {!Array<!Array<(!RegExp|string)>>} inputs A serial collection of answers. Each answer will be ended with an EOL character (unless specified otherwise in the last argument). For example, pass `[[/question/, 'answer'], [/question2/, 'answer2]]`.
 * @param {stream.Writable=} [log] A stream to which to write both data from readable, and the passed answer. Default `null`.
 * @param {string=} [eol] The line feed to pass at the end of each answer. By default, _ForkFeed_ uses `os.EOL`, and when readline interface is used, there won't be `\r` in answers on Windows, but this can be useful for logging.
 */
function forkfeed(readable, stdin, inputs, log, eol) {
  return $forkfeed(readable, stdin, inputs, log, eol)
}

module.exports = forkfeed

/* documentary types/index.xml namespace */
/**
 * @typedef {import('stream').Writable} stream.Writable
 * @typedef {import('stream').Readable} stream.Readable
 */
