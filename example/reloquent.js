import { fork } from 'spawncommand'
import Catchment from 'catchment'
import forkfeed from '../'

(async () => {
  const p = fork('example/q', [], {
    stdio: 'pipe',
  })
  const log = new Catchment({
    binary: true,
  })
  forkfeed(p.stdout, p.stdin, [
    [/question/, 'answerasd\r'],
    [/hello/, 'world\r'],
  ], log)
  const { stdout } = await p.promise
  console.log('answers: [%s]', stdout.trim())

  log.end()
  console.log([(await log.promise).toString()])
})()