const ask = require('reloquent')

;(async () => {
  const p = await ask({
    q1: 'question',
    q2: 'hello',
  })
  console.log(p)
})()