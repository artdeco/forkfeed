<div align="center">

# ForkFeed

%NPM: forkfeed%
<pipeline-badge />
</div>

`forkfeed` is a function that passes answer values to a child process on data. For example, if a fork was spawned which requires user interaction, this module can be used to pass answers to it according to the seen data from `stdout`.

```sh
yarn add forkfeed
npm i forkfeed
```

## Table Of Contents

%TOC%

%~%