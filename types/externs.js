/**
 * @fileoverview
 * @externs
 */
/* typal types/api.xml externs */
/** @const */
var _forkfeed = {}
/**
 * Write data to the `writable` when data from the `readable` matches the regexp.
 * @typedef {function(!stream.Readable,!stream.Writable,!Array<!Array<(!RegExp|string)>>,stream.Writable=,string=)}
 */
_forkfeed.forkfeed
